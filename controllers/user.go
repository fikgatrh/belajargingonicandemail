package controllers

import (
	"belajarGinGonic/middlewares"
	"belajarGinGonic/models"
	"belajarGinGonic/user"
	"belajarGinGonic/utils"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
	"gopkg.in/gomail.v2"
	"log"
	"net/http"
	"os"
)

const (
	CONFIG_SMTP_PORT = 587
	CONFIG_SMTP_HOST = "smtp.gmail.com"
)

//const CONFIG_EMAIL = "fikrigatra4@gmail.com"
//const CONFIG_PASSWORD = "setyourpassword"

type UserController struct {
	userUsecase user.UserService
}

func CreateUserController(userUsecase user.UserService) {
	inDB := UserController{userUsecase}
	router := gin.New()
	router.Use(gin.Recovery(), middlewares.Logger())

	router.POST("/user/login", inDB.Login)
	router.POST("/user", inDB.createUser)

	//ini adalah bagian untuk memasang si Authentikasi Token
	v2 := router.Group("/auth")
	v2.Use(middlewares.Auth())
	v2.GET("/users", inDB.getAll)

	//router.GET("/users", inDB.getAll)
	router.GET("/user/:id", inDB.getById)
	router.PATCH("/user/update/:id", inDB.updateData)
	router.DELETE("/user/:id", inDB.deleteData)

	router.Run(":9091")

}

func (u *UserController) createUser(c *gin.Context) {
	//var input models.User
	//data := models.User{
	//	Email:    input.Email,
	//	FullName: input.FullName,
	//	Password: input.Password,
	//}

	data, err := getData()
	if err != nil {
		fmt.Printf("[UserController.createUser] Error when call function getData with error: %v\n", err)
	}

	//1. ngedecode dahulu jsonnya
	err = c.ShouldBindJSON(&data)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error(),})
		return
	}

	validate := validator.New()
	errs := validate.Struct(data)

	if errs != nil {
		fmt.Println(errs)
	}

	isValid := utils.ValidateUser(c, data)
	if !isValid {
		return
	}

	checkEmail := utils.CheckValidMail(data.Email)

	if !checkEmail {
		fmt.Println("[UserHandler] Insert User error because Email address doesn't have @")
		c.JSON(http.StatusBadRequest, utils.ErrorMessageResponse("Email address doesn't have @"))
		return
	}

	sendEmail(data.Email)

	//from := mail.NewEmail("Me", "haryonofikri1@gmail.com")
	//to := mail.NewEmail("User", data.Email)
	//subject := "Sending with gatra dong"
	//plainTextContext := "and easy to do anywhere"
	//htmlContent := "<strong>and easy very easy aye aye"
	//message := mail.NewSingleEmail(from, subject, to, plainTextContext,htmlContent)
	//client := sendgrid.NewSendClient(os.Getenv("SENDGRID_API_KEY"))
	//response, err := client.Send(message)
	//if err != nil {
	//	log.Println(err)
	//}else {
	//	fmt.Println(response)
	//}

	userHashed, _ := utils.HashPassword(c, data)

	//2. dipanggil usecasenya
	_, err = u.userUsecase.CreateUser(userHashed)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"message": err.Error(),})
	}

	fmt.Println(data)

	c.JSON(http.StatusOK, gin.H{
		"status":   "OK",
		"messages": "SUCCESS",
	})

}

func (u *UserController) getAll(c *gin.Context) {
	var result gin.H
	message := "SUCCESS"
	status := "OK"

	dataUser, err := u.userUsecase.GetAll()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"Ooops something error": err.Error(),})
		fmt.Printf("[UserController.getAll] Error when request data to usecase with error: %v\n", err)
	}

	if len(*dataUser) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else if len(*dataUser) > 0 {
		result = gin.H{
			"jumlah":   len(*dataUser),
			"status":   status,
			"messages": message,
			"users":    dataUser,
		}
	}

	c.JSON(http.StatusOK, result)
}

func (u *UserController) getById(c *gin.Context) {
	var result gin.H

	id := c.Param("id")
	fmt.Println(id)

	dataUser, err := u.userUsecase.GetById(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Record not found!"})
		fmt.Printf("[UserController.getById]Error when query get data with error:%v\n", err)
		return
	} else {
		result = gin.H{
			"count": 1,
			"data":  dataUser,
		}
	}
	fmt.Println(dataUser.Email)
	c.JSON(http.StatusOK, result)
}

func (u *UserController) updateData(c *gin.Context) {

	id := c.Param("id")
	_, err := u.userUsecase.GetById(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Record not found!"})
		fmt.Printf("[UserController.getById]Error when query get data with error:%v\n", err)
		return
	}
	fmt.Println("ini id ke - ", id)

	//dataUser,err := getData()
	//if err!= nil{
	//	fmt.Printf("[UserControllupdateDataer.updateData] Error when call function getData with error: %v\n",err)
	//}
	dataUser := models.User{}

	err = c.ShouldBindJSON(&dataUser)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error(),})
		return
	}
	fmt.Println(dataUser)

	data, err := u.userUsecase.UpdateUser(id, &dataUser)
	if err != nil {
		fmt.Printf("[UserController.UpdateUser]Error when query update data with error:%v\n", err)
		return
	}
	fmt.Println(data)

	c.JSON(http.StatusOK, gin.H{"data": data,})
}

func (u *UserController) deleteData(c *gin.Context) {
	var result gin.H

	id := c.Param("id")
	_, err := u.userUsecase.GetById(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Record not found!"})
		fmt.Printf("[UserController.getById]Error when query get data with error:%v\n", err)
		return
	}
	fmt.Println("ini id ke - ", id)

	_, err = u.userUsecase.DeleteUser(id)
	if err != nil {
		fmt.Printf("[UserController.DeleteData]Error when query delete data with error:%v\n", err)
		return
	} else {
		result = gin.H{
			"status":   "OKE",
			"messages": "Data deleted successfully",
		}
	}
	c.JSON(http.StatusOK, result)

}

func (u *UserController) Login(c *gin.Context) {
	//var validate *validator.Validate
	var dataUser models.User
	//var user  = models.User{}
	var JWT models.TokenStruct

	// parse body post data
	//c.ShouldBindJSON(&dataUser)
	//if err := c.ShouldBindJSON(&dataUser); err != nil {
	//	c.JSON(http.StatusUnprocessableEntity, "Invalid json provided")
	//	return
	//}
	err := c.ShouldBindJSON(&dataUser)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error(),})
		return
	}
	//if user.Email != dataUser.Email || user.Password != dataUser.Password {
	//	c.JSON(http.StatusUnauthorized, "Please provide valid login details")
	//	return
	//}

	//validate = validator.New()
	//validateError := validate.Struct(dataUser)
	//
	//if validateError != nil {
	//	c.JSON(http.StatusBadRequest, utils.ErrorMessageResponse(validateError.Error()))
	//	return
	//}

	checkEmail := utils.CheckValidMail(dataUser.Email)
	if !checkEmail {
		c.JSON(http.StatusUnauthorized, utils.ErrorMessageResponse("Email address is invalid"))
		return
	}

	password := dataUser.Password

	userData, msg, err := u.userUsecase.Login(dataUser.Email)
	if msg != "" {
		c.JSON(http.StatusBadRequest, msg)
		return
	}
	if err != nil {
		c.JSON(http.StatusUnauthorized, utils.ErrorMessageResponse("The user doesnt exists"))
		return
	}
	isValid := utils.CheckPasswordHash(userData.Password, []byte(password))
	if isValid {
		token, err := utils.GenerateToken(userData)
		if err != nil {
			c.JSON(http.StatusInternalServerError, utils.ErrorMessageResponse("The user doesnt exists"))
			fmt.Println("[UserHandler.LoginUser]error occured while generate token ", err.Error())
		}
		JWT.Token = token
		c.JSON(http.StatusOK, JWT)
		return
		//utils.SuccessDataResponse(c, http.StatusOK, JWT)
		//utils.HandleSuccess(resp, http.StatusOK, jwt)
	} else {
		c.JSON(http.StatusUnauthorized, utils.ErrorMessageResponse("Unauthorized"))
	}

}

func sendEmail(sendEmail string) {

	var toEmail = sendEmail
	email := os.Getenv("CONFIG_EMAIL")
	password := os.Getenv("CONFIG_PASSWORD")
	mailer := gomail.NewMessage()
	mailer.SetHeader("From", email)
	mailer.SetHeader("To", toEmail)
	mailer.SetAddressHeader("Cc", "tralalala@gmail.com", "Tra Lala La")
	mailer.SetHeader("Subject", "Test Mail")
	mailer.SetBody("text/html", fmt.Sprintf("Hello %s!", toEmail))

	dialer := gomail.NewDialer(
		CONFIG_SMTP_HOST,
		CONFIG_SMTP_PORT,
		email,
		password,
	)

	err := dialer.DialAndSend(mailer)
	if err != nil {
		fmt.Println("kesini mih kiirmi emaildsada")
		log.Fatal(err)
	}

	log.Println("Mail sent!!")
}

func getData() (*models.User, error) {
	var input models.User
	data := models.User{
		Email:    input.Email,
		FullName: input.FullName,
		Password: input.Password,
	}
	return &data, nil
}
