package elasticsearch

import (
	"github.com/olivere/elastic/v7"
	"time"
)

var(
	Client esClientInterface = &esClient{}
)

type esClientInterface interface {

}

type esClient struct {
	client *elastic.Client
}

func esGetClient() {
	esClient := esClient{}
	var err error
	esClient.client, err = elastic.NewClient(
		elastic.SetURL("http://127.0.1.1:9200"),
		//elastic.SetSniff(false),
		elastic.SetHealthcheckInterval(10*time.Second),
		//elastic.SetGzip(true),
		//elastic.SetErrorLog(log.New(os.Stderr, "ELASTIC ", log.LstdFlags)),
		//elastic.SetInfoLog(log.New(os.Stdout, "", log.LstdFlags)),
		//elastic.SetHeaders(http.Header{
		//	"X-Caller-Id": []string{"..."},
		//}),
	)
	if err != nil {
		panic(err)
	}
}
