package utils

import (
	"belajarGinGonic/models"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	"os"
	"regexp"
	"time"
)

// ErrorMessageResponse Request response object ready for errors.
func ErrorMessageResponse(message string) gin.H {
	return gin.H{
		"status":  "ERROR",
		"message": message,
	}
}

// SuccessMessageResponse Request response object ready for success.
func SuccessMessageResponse(message string) gin.H {
	return gin.H{
		"status": "success",
		"data": gin.H{
			"message": message,
		},
	}
}

// SuccessDataResponse gives a successful request response.
func SuccessDataResponse(data gin.H) gin.H {
	return gin.H{
		"status": "success",
		"data":   data,
	}
}

func CheckPasswordHash(hashedPwd string, plainPwd []byte) bool {
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		return false
	}
	return true
}

func CheckValidMail(email string) bool {
	regex := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	return regex.MatchString(email)
}

func ValidateUser(c *gin.Context, user *models.User) bool {
	if user.Email == "" {
		c.JSON(http.StatusBadRequest, ErrorMessageResponse("Email cannot be empty"))
		fmt.Print("[Validate]Error When Validate Email")
		return false
	}

	if user.FullName == "" {
		c.JSON(http.StatusBadRequest, ErrorMessageResponse("Full Name cannot be empty"))
		fmt.Print("[Validate]Error When Validate Email")
		return false
	}

	if user.Password == "" {
		//HandleError(resp, http.StatusBadRequest, "Password cannot be empty")
		c.JSON(http.StatusBadRequest, ErrorMessageResponse("Password cannot be empty"))
		fmt.Print("[Validate]Error When Validate Email")
		return false
	}

	return true
}

func GenerateToken(user *models.User) (string, error) {
	var err error
	secret := os.Getenv("SECRET")

	token := jwt.New(jwt.SigningMethodHS256)

	token.Claims = jwt.MapClaims{
		"id":    user.ID,
		"email": user.Email,
		"exp":   time.Now().Add(time.Hour * 24 * 90).Unix(),
	}

	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		log.Fatal(err)
	}
	return tokenString, nil
}

func HashPassword(c *gin.Context, user *models.User) (*models.User, error) {
	if len(user.Password) == 0 {
		return nil, errors.New("password should not be empty!")
	}
	bytePassword := []byte(user.Password)
	// Make sure the second param `bcrypt generator cost` between [4, 32)
	passwordHash, err := bcrypt.GenerateFromPassword(bytePassword, 10)
	if err != nil {
		c.JSON(http.StatusUnauthorized, ErrorMessageResponse("Email address is invalid"))
		fmt.Printf("[UserController.SetPassword] Error when generate password with error: %v\n", err)
		return nil, nil
	}
	user.Password = string(passwordHash)
	return user, nil
}
