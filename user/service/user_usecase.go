package service

import (
	"belajarGinGonic/models"
	"belajarGinGonic/user"
	"fmt"
)

type UserUsecaseImpl struct {
	userRepo user.UserRepo
}

func CreateUserUsecaseImpl(userRepo user.UserRepo) user.UserService {
	return &UserUsecaseImpl{userRepo}
}

func (u UserUsecaseImpl) CreateUser(data *models.User) (*models.User, error) {
	return u.userRepo.CreateUser(data)
}

func (u UserUsecaseImpl) GetAll() (*[]models.User, error) {
	return u.userRepo.GetAll()
}

func (u UserUsecaseImpl) GetById(id string) (*models.User, error) {
	return u.userRepo.GetById(id)
}

func (u UserUsecaseImpl) UpdateUser(id string, user *models.User) (*models.User, error) {
	return u.userRepo.UpdateUser(id, user)
}

func (u UserUsecaseImpl) DeleteUser(id string) (*models.User, error) {
	return u.userRepo.DeleteUser(id)
}

func (u UserUsecaseImpl) CheckEmail(email string) bool {
	return u.userRepo.CheckEmail(email)
}

func (u UserUsecaseImpl) Login(email string) (*models.User, string, error) {
	checkEmail := u.userRepo.CheckEmail(email)

	if checkEmail == false {
		return nil, "Email Not Found", nil
	}
	fmt.Println(checkEmail)
	dataUser, err := u.userRepo.Login(email)
	return dataUser, "", err
}

