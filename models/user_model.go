package models

type User struct {
	ID       int    `gorm:"primary_key" json:"id"`
	Email    string `gorm:"type:varchar(50);unique_index" json:"email,omitempty" validate:"required,email"`
	FullName string `gorm:"type:varchar(50)" json:"full_name,omitempty" validate:"required,min=5"`
	Password string `gorm:"type:varchar(255)" json:"password,omitempty" validate:"required,min=3"`
}

func (u User) TableName() string {
	return "tb_user"
}
