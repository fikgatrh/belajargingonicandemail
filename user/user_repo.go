package user

import "belajarGinGonic/models"

type UserRepo interface {
	CreateUser(user *models.User) (*models.User, error)
	GetAll() (*[]models.User, error)
	GetById(id string) (*models.User, error)
	UpdateUser(id string, user *models.User) (*models.User, error)
	DeleteUser(id string) (*models.User, error)
	Login(email string) (*models.User, error)
	CheckEmail(email string) bool
}
