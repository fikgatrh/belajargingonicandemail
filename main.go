package main

import (
	"belajarGinGonic/config"
	"belajarGinGonic/controllers"
	"belajarGinGonic/user/repo"
	"belajarGinGonic/user/service"
	"github.com/gin-gonic/gin"
	"github.com/subosito/gotenv"
	"io"
	"os"
)

func init()  {
	gotenv.Load()
}

func setupLogOutput()  {
	f,_:= os.Create("gin.Log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func main()  {
	db := config.ConnectDB()
	defer db.Close()

	setupLogOutput()

	userRepo:=repo.CreateUserRepoImpl(db)
	userUsecase:= service.CreateUserUsecaseImpl(userRepo)
	controllers.CreateUserController(userUsecase)
}
