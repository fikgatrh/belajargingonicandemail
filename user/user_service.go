package user

import "belajarGinGonic/models"

type UserService interface {
	CreateUser(user *models.User) (*models.User, error)
	GetAll() (*[]models.User, error)
	GetById(id string) (*models.User, error)
	UpdateUser(id string, user *models.User) (*models.User, error)
	DeleteUser(id string) (*models.User, error)
	Login(email string) (*models.User, string, error)
	CheckEmail(email string) bool
}
