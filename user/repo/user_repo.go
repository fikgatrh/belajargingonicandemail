package repo

import (
	"belajarGinGonic/models"
	"belajarGinGonic/user"
	"fmt"
	"github.com/jinzhu/gorm"
)

type UserRepoImpl struct {
	db *gorm.DB
}

func CreateUserRepoImpl(db *gorm.DB) user.UserRepo {
	return &UserRepoImpl{db}
}

func (u *UserRepoImpl) CreateUser(data *models.User) (*models.User, error) {
	tx := u.db.Begin()
	err := tx.Debug().Create(&data).Error
	if err != nil {
		tx.Rollback()
		return nil, fmt.Errorf("[UserRepoImpl.CreateUser]Error when query insert data with error: %w\n", err)
	}
	tx.Commit()

	return data, nil
}

func (u *UserRepoImpl) GetAll() (*[]models.User, error) {
	var dataUser []models.User

	err := u.db.Debug().Find(&dataUser).Error
	if err != nil {
		return nil, fmt.Errorf("[UserRepoImpl.GetAll] Error when query get all data with error : %w\n", err)
	}
	return &dataUser, nil
}

func (u *UserRepoImpl) GetById(id string) (*models.User, error) {
	var dataUser models.User

	err := u.db.Debug().Where("id = ?", id).First(&dataUser).Error
	if err != nil {
		return nil, fmt.Errorf("[UserRepoImpl.GetById] Error when query get by ID with error : %w\n", err)
	}
	return &dataUser, nil
}

func (u *UserRepoImpl) UpdateUser(id string, user *models.User) (*models.User, error) {
	err := u.db.Debug().Model(&user).Where("id = ?", id).Updates(user).Error
	if err != nil {
		return nil, fmt.Errorf("[UserRepoImpl.UpdateUser] Error when query update data with error : %w\n", err)
	}
	return user, nil
}

func (u *UserRepoImpl) DeleteUser(id string) (*models.User, error) {
	var dataUser models.User

	err := u.db.Debug().Where("id = ?", id).Delete(&dataUser).Error
	if err != nil {
		return nil, fmt.Errorf("[UserRepoImpl.DeleteUser] Error when query delete data with error : %w\n", err)
	}
	return &dataUser, nil
}

func (u *UserRepoImpl) CheckEmail(email string) bool {
	var total int

	u.db.Debug().Table("tb_user").Where("email = ?", email).Count(&total)
	fmt.Println(total)

	if total > 0 {
		return true
	}
	return false
}

func (u *UserRepoImpl) Login(email string) (*models.User, error) {
	var data models.User
	err := u.db.Debug().Where("email=?", email).Find(&data).Error
	fmt.Println("data email", data.Email)
	if err != nil {
		return nil, fmt.Errorf("[UserRepoImpl.Login] Error when query select email and password with error: %w\n", err)
	}

	return &data, nil
}

