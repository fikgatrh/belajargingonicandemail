package config

import (
	"belajarGinGonic/models"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"os"
)

var db *gorm.DB

func ConnectDB() *gorm.DB {
	conn := os.Getenv("POSTGRES_GIN")
	db, err := gorm.Open("postgres", conn)

	if err != nil {
		fmt.Println("[CONFIG.ConnectDB] error when connect to database")
		log.Fatal(err)
	}else {
		fmt.Println("SUCCES CONNECT TO DATABASE")
	}

	models.InitTable(db)

	return db
}
