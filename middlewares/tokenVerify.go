package middlewares

import (
	"belajarGinGonic/utils"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"strings"
)

func Auth() gin.HandlerFunc {
	return func(c *gin.Context) {

		bearer := c.Request.Header.Get("Authorization")
		jwtParts := strings.Split(bearer, " ")
		if bearer != "" {
			if len(jwtParts) == 2 {
				tokenString := jwtParts[1]

				token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						return nil, fmt.Errorf("Unexpected signing method: %v")
					}
					return []byte(os.Getenv("SECRET")), nil
				})
				if err != nil {
					c.JSON(http.StatusUnauthorized, utils.ErrorMessageResponse("Ooppss , something when wrong"))
					c.Abort()
					return
				}
				spew.Dump(token)
				if token.Valid {
					//c.JSON(http.StatusOK, utils.SuccessMessageResponse("token verified"))
					fmt.Println("token verified")
				} else {
					//_ = gin.H{
					//	"message": "Invalid Token",
					//	"error":   err.Error(),
					//}
					c.JSON(http.StatusUnauthorized, utils.ErrorMessageResponse("Invalid Token"))
					return
					//c.JSON(http.StatusUnauthorized, result)
				}
			} else {
				c.JSON(http.StatusUnauthorized, utils.ErrorMessageResponse("Invalid Token"))
				c.Abort()
				return
			}
		} else {
			c.JSON(http.StatusUnauthorized, utils.ErrorMessageResponse("You need to be authorized to access this route"))
			c.Abort()
			return
		}
	}
}
